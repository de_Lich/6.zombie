﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Weapon : MonoBehaviour // система оружия
{
    [SerializeField] Camera FPCamera;

    [SerializeField] float range = 100f;
    [SerializeField] float damage = 30f;

    [SerializeField] ParticleSystem muzzleFlash;
    [SerializeField] GameObject hitEffect;

    [SerializeField] Ammo ammoSlot;
    [SerializeField] AmmoType ammoType;
    [SerializeField] AmmoPower ammoPower;

    [SerializeField] float timeBetweenShots = .5f;
    [SerializeField] TextMeshProUGUI ammoText;
    [SerializeField] WeaponsSwitcher weaponType;
    [SerializeField] GameObject RocketAmmoPrefab;
    [SerializeField] float RocketVelocity = 20f;

    bool canShoot = true;

    private void OnEnable()
    {
        canShoot = true;
    }
    void Update()
    {
        ShootingMethod();
        DisplayAmmo();
    }

    private void ShootingMethod() // варианты стрельбы в зависимости от типа 
    {
        if (weaponType.currentWeapon == 0)
        {
            if (Input.GetMouseButtonDown(0) && canShoot == true)
            {
                StartCoroutine(Shoot());
                
            }
        }
        if (weaponType.currentWeapon == 1)
        {
            if (Input.GetMouseButtonDown(0) && canShoot == true)
            {
                StartCoroutine(Shoot());
            }
        }
        if (weaponType.currentWeapon == 2)
        {
            if (Input.GetMouseButtonDown(0) && canShoot == true)
            {
                StartCoroutine(Shoot());
            }
        }
        if (weaponType.currentWeapon == 3)
        {
            if(Input.GetMouseButton(0) && canShoot == true)
            {
                StartCoroutine(Shoot());
            }
        }
        if (weaponType.currentWeapon == 4)
        {
            if (Input.GetMouseButtonDown(0) && canShoot == true)
            {
                StartCoroutine(RocketCoroutine());
            }
        }
        if (weaponType.currentWeapon == 5)
        {
            if (Input.GetMouseButtonDown(0) && canShoot == true)
            {
                StartCoroutine(RocketCoroutine());
                FindObjectOfType<AudioManager>().Play("GranadeBlow");
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && canShoot == true)
            {
                StartCoroutine(Shoot());
            }
        }
    }

    private void DisplayAmmo() // отображение количества боеприпасов
    {
        int currentAmmo = ammoSlot.GetCurrentAmmo(ammoType);
        ammoText.text = currentAmmo.ToString();
    }

    IEnumerator Shoot()  // корутина стрельбы
    {
        canShoot = false;
        if (ammoSlot.GetCurrentAmmo(ammoType) > 0)
        {
            ammoSlot.ReduceCurrentAmmo(ammoType);
            FindObjectOfType<AudioManager>().Play("Shoot");
            PlayMuzzleFlash();
            ProcessRaycast();
        }
        yield return new WaitForSeconds(timeBetweenShots);
        canShoot = true;
    }
    IEnumerator ShootFromShotGun() // крутина выстрела из дробовика
    {
        canShoot = false;
        if (ammoSlot.GetCurrentAmmo(ammoType) > 0)
        {
            ammoSlot.ReduceCurrentAmmo(ammoType);
            PlayMuzzleFlash();
            ProcessMassRaycast();
        }
        yield return new WaitForSeconds(timeBetweenShots);
        canShoot = true;
    }
    IEnumerator RocketCoroutine() // корутина выстрела ракетницы
    {
        canShoot = false;
        if (ammoSlot.GetCurrentAmmo(ammoType) > 0)
        {
            ammoSlot.ReduceCurrentAmmo(ammoType);
            PlayMuzzleFlash();
            RocketLauch();
        }
        yield return new WaitForSeconds(timeBetweenShots);
        canShoot = true;
    }

    private void PlayMuzzleFlash()// огонь выстрела
    {
        muzzleFlash.Play();
    }

    private void ProcessRaycast() // процесс стрельбы
    {
        RaycastHit hit;
        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            CreateHitImpact(hit);
            EnemyHealth target = hit.transform.GetComponent<EnemyHealth>();
            if (target == null) return;
            target.TakeDamage(damage);
        }
        else { return; }
    }
    private void ProcessMassRaycast() // процесс стрельбы из дробовика (в разработке)
    {

        RaycastHit hit;
        RaycastHit hit2;
        RaycastHit hit3;

        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            CreateHitImpact(hit);
            EnemyHealth target = hit.transform.GetComponent<EnemyHealth>();
            if (target == null) return;
            target.TakeDamage(damage);
        }
        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward + new Vector3(.5f,0f,0f), out hit2, range))
        {
            CreateHitImpact(hit2);
            Debug.Log(hit2.transform.name);
            EnemyHealth target = hit2.transform.GetComponent<EnemyHealth>();
            if (target == null) return;
            target.TakeDamage(damage/2);
        }
        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward + new Vector3(-.5f, 0f, 0f), out hit3, range))
        {
            CreateHitImpact(hit3);
            Debug.Log(hit3.transform.name);
            EnemyHealth target = hit3.transform.GetComponent<EnemyHealth>();
            if (target == null) return;
            target.TakeDamage(damage/2);
        }
        else { return; }
    }
    private void CreateHitImpact(RaycastHit hit)// куда попала пуля
    {
        GameObject impact = Instantiate(hitEffect,hit.point, Quaternion.LookRotation(hit.normal));
        Destroy(impact.gameObject, 0.1f);
    }
    private void RocketLauch() // выстрел ракеты или гранаты
    {
        GameObject newRocket = Instantiate(RocketAmmoPrefab, transform.position, transform.rotation); //todo корректное положение снаряда
        newRocket.GetComponent<Rigidbody>().velocity = transform.forward * RocketVelocity;
    }
    public void IncreaseDamage(AmmoType ammoType, float damageInc) // увеличение урона. сделать увеличение кокретного типа оружия
    {
        // проверка на тип оружия
        // улучшение переносилось на следующий уровень
        damage += damageInc;
    }
}
