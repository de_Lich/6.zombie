﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketShell : MonoBehaviour
{
    [SerializeField] float damage = 100f;
    [SerializeField] float radius = 100f;
    [SerializeField] float force = 100f;
    [SerializeField] ParticleSystem explosion;
    bool hasExploded = false;
    void OnCollisionEnter(Collision collision) //todo  некорректный взрыв
    {
        Instantiate(explosion, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider nearbyObject in colliders)
        {
            EnemyHealth enemyHealth = nearbyObject.GetComponent<EnemyHealth>();
            if (enemyHealth !=null && !hasExploded)
            {
                collision.gameObject.GetComponent<EnemyHealth>(); //?
                enemyHealth.TakeDamage(damage);
                hasExploded = true;
            }
        }
        explosion.Stop();
        Destroy(gameObject);
    }
}
