﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeLauncher : MonoBehaviour
{
    [SerializeField] float radius = 20f;
    [SerializeField] float damage = 100f;
    public float delay = 3f;
    float countDown;

    [SerializeField] GameObject explosion;

    bool hasExploded = false;
    void Start()
    {
        countDown = delay;
    }
    void Update()
    {
        countDown -= Time.deltaTime;
        if (countDown <=0f && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    public void Explode()
    {
        Instantiate(explosion, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider nearbyObject in colliders)
        {
            EnemyHealth enemyHealth = nearbyObject.GetComponent<EnemyHealth>();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage(damage);
            }
        }
        Collider[] collidersForPlayer = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider nearbyObject in collidersForPlayer)
        {
            PlayerHealth playerHealth = nearbyObject.GetComponent<PlayerHealth>();
            if(playerHealth != null)
            {
                playerHealth.TakeDamage(damage/2);
            }
        }

        Destroy(gameObject);
    }
}
