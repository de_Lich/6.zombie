﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinCondition : MonoBehaviour // условие победы
{
    [SerializeField] GameObject winText;
    [SerializeField] GameObject winRules;
    [SerializeField] Text actialTimerUp;
    [SerializeField] Text actialTimerDown;

    [SerializeField] int killNorm = 0; 
    [SerializeField] int killFast = 0; 
    [SerializeField] int killFat = 0; 
    [SerializeField] int killBoss = 0;

    [SerializeField] float timerLimit = 1f;
    [SerializeField] float timerUp = 0f;
    [SerializeField] float timerDown = 3000f;

    int norm, fast, fat, boss = 0;

    public void Update()
    {
        timerUp += Time.deltaTime;
        timerDown -= Time.deltaTime;
        EnemySpawn enemy = FindObjectOfType<EnemySpawn>();
        norm = enemy.normKill;
        fast = enemy.fatKill;
        fat = enemy.fastKill;
        boss = enemy.bossKill;
        DisplayTimers();
        WinRules();
        if(timerUp >= 5f)
        {
            winRules.SetActive(false);
        }
    }

    private void DisplayTimers()
    {
        string minutes = ((int)timerDown / 60).ToString("00");
        string seconds = ((int)timerDown % 60).ToString("00");

        string minutesUp = ((int)timerUp / 60).ToString();
        string secondsUp = ((int)timerUp % 60).ToString("00");

        actialTimerUp.text = minutesUp + ":" + secondsUp;
        actialTimerDown.text = minutes + ":" + seconds;
        if(timerDown <= 0)
        {
            timerDown = 0f;
            FindObjectOfType<PlayerHealth>().KillPlayer();
        }
        if(timerUp >= timerLimit)
        {
            timerUp = timerLimit;
        }
    }

    public void WinRules() // условия для победы, регулируются в unity -> enemy spawner
    {
        if(timerUp >= timerLimit && norm == killNorm && fast == killFast && fat == killFat && boss == killBoss)
        {
            Debug.Log("DONE");
            Invoke("Win", 2f);
        }
    }
    public void Win() 
    {
        winText.SetActive(true);
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
