﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour // UI / unity -> gamesession
{
    void Start() 
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void ReloadGame() // перезагрузка текущего уровня
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        DeathHandler dis = FindObjectOfType<DeathHandler>();
        dis.gameOverCanvas.enabled = false;
        dis.menuCanvas.enabled = false;
        Time.timeScale = 1;
    }
    public void ResumeGame() // пауза/возобновление
    {
        SceneManager.GetActiveScene();
        DeathHandler dis = FindObjectOfType<DeathHandler>();
        dis.menuCanvas.enabled = false;
        Time.timeScale = 1;
        WeaponsSwitcher weapons = FindObjectOfType<WeaponsSwitcher>();
    }

    public void QuitGame() 
    {
        Application.Quit();
    }
    public void StartGame() // начало игры
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void Options()
    {

    }

    public void NextLevel() // переход на следующий уровень
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int sceneIndex = currentSceneIndex + 1;
        SceneManager.LoadScene(sceneIndex);
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
