﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler : MonoBehaviour  // UI после смерти игрока
{
    [SerializeField] public Canvas gameOverCanvas;
    [SerializeField] public Canvas menuCanvas;

    bool canOpen = true;

    private void Start()
    {
        gameOverCanvas.enabled = false;
        menuCanvas.enabled = false;
    }
    public void Update()
    {
        if(canOpen)
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OpenMenu();
        }
    }

    private void OpenMenu()
    {
        menuCanvas.enabled = true;
        Time.timeScale = 0;
        FindObjectOfType<WeaponsSwitcher>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void HandledDeath()
    {
        canOpen = false;
        gameOverCanvas.enabled = true;
        Time.timeScale = 0;
        FindObjectOfType<WeaponsSwitcher>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
