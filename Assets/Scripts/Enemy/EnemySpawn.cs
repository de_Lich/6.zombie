﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemySpawn : MonoBehaviour  //система спавна мобов
{
    [SerializeField] Terrain Worldterrain;  // координаты спавна
    public static float TerrainLeft, TerrainRight, TerrainTop, TerrainBottom, TerrainWidth, TerrainLength, TerrainHeight;
    [SerializeField] float timebetweenSpawn = 2f;

    [SerializeField] EnemyAI fatEnemyPrefab;  // префабы мобов
    [SerializeField] EnemyAI fastEnemyPrefab;
    [SerializeField] EnemyAI normalEnemyPrefab;
    [SerializeField] EnemyAI BossEnemyPrefab;
    [SerializeField] Transform parent;

    [SerializeField] int score = 0;   // счетчики фрагов 

    private int enemyKill;

    [SerializeField] TextMeshProUGUI textNormKills;
    [SerializeField] TextMeshProUGUI textFastKills;
    [SerializeField] TextMeshProUGUI textFatKills;
    [SerializeField] TextMeshProUGUI textBossKills;

    [HideInInspector]
    public int normKill,fastKill, fatKill, bossKill = 0;

    void Start()
    {
        TerrainLeft = Worldterrain.transform.position.x;
        TerrainBottom = Worldterrain.transform.position.z;
        TerrainWidth = Worldterrain.terrainData.size.x;
        TerrainLength = Worldterrain.terrainData.size.z;
        TerrainHeight = Worldterrain.terrainData.alphamapHeight;
        TerrainRight = TerrainLeft + TerrainWidth;
        TerrainTop = TerrainBottom + TerrainLength;

        StartCoroutine(EnemySpawner(0f));
    }
    IEnumerator EnemySpawner(float AddedHeigth)  // это спавн мобов 
    {
        float terrainHeight = 0f;
        RaycastHit hit;
        float randomPositionX, randomPositionY, randomPositionZ;
        Vector3 randomPosition = Vector3.zero;
        while (score <= 500)
        {
            score++;

            randomPositionX = Random.Range(TerrainLeft, TerrainRight);
            randomPositionZ = Random.Range(TerrainBottom, TerrainTop);

            if(Physics.Raycast(new Vector3(randomPositionX, 999f, randomPositionZ), Vector3.down, out hit, Mathf.Infinity))
            {
                terrainHeight = hit.point.y;
            }
            randomPositionY = terrainHeight + AddedHeigth;

            randomPosition = new Vector3(randomPositionX, randomPositionY, randomPositionZ);

            var randomEnemy = UnityEngine.Random.Range(1, 10);

            if (randomEnemy <= 1)
            {
                EnemyAppear(fatEnemyPrefab, randomPosition);
            }

            else if (randomEnemy >= 2 && randomEnemy <= 4)
            {
                EnemyAppear(fastEnemyPrefab, randomPosition);
            }

            else if (randomEnemy >= 5)
            {
                EnemyAppear(normalEnemyPrefab, randomPosition);
            }
            yield return new WaitForSeconds(timebetweenSpawn);
        }
    }

    private void EnemyAppear(EnemyAI enemy, Vector3 randomPosition)  // появление мобов
    {
        EnemyAI newEnemy = Instantiate(enemy, randomPosition, Quaternion.identity);
        newEnemy.transform.parent = parent;
    }

    public void EnemyScore(EnemyType enemy) // подсчет трупов
    {
        switch (enemy)
        {
            case EnemyType.Normal:
                normKill++;
                Debug.Log("Normal zombie kills: " + normKill);
                break;
            case EnemyType.Fat:
                fatKill++;
                Debug.Log("Fat zombie kills: " + fatKill);
                break;
            case EnemyType.Fast:
                fastKill++;
                Debug.Log("Fast zombie kills: " + fastKill);
                break;
            case EnemyType.Boss:
                bossKill++;
                Debug.Log("Boss zombie kills: " + bossKill);
                break;
            default:
                break;
        }
        enemyKill++;

        int normKills = normKill;
        int fastKills = fastKill;
        int fatKills = fatKill;
        int bossKills = bossKill;

        textNormKills.text = normKills.ToString();
        textFastKills.text = fastKills.ToString();
        textFatKills.text = fatKills.ToString();
        textBossKills.text = bossKills.ToString();

        if (enemyKill == 10 || enemyKill == 20 || enemyKill == 30)
        {
            EnemyAI boss = Instantiate(BossEnemyPrefab, transform.position, Quaternion.identity);
            boss.transform.parent = parent;
            Debug.Log("BOOSSS!!!");           
        }
    }
}
