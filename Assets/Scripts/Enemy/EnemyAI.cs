﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour  // механика моба
{
    NavMeshAgent navMeshAgent;
    [SerializeField] float range = 10f;
    [SerializeField] float turnSpeed = 5f;
    float distanceToTarget = Mathf.Infinity;
    bool isProvoked = false;
    EnemyHealth health;
    Transform target;
    float countDown = 3f;
    
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        health = GetComponent<EnemyHealth>();
        target = FindObjectOfType<PlayerHealth>().transform;
    }


    void Update()
    {
        countDown -= Time.deltaTime;
        {
            if (health.IsDead())
            {
                enabled = false;
                navMeshAgent.enabled = false;
                GetComponent<CapsuleCollider>().enabled = false;
            }
            distanceToTarget = Vector3.Distance(target.position, transform.position);
            if (isProvoked)
            {
                EngageTarget();
            }
            else if (distanceToTarget <= range)
            {
                isProvoked = true;
            }
        }
    }

    public void OnDamageTaken()
    {
        isProvoked = true;
    }
    private void EngageTarget()  // триггер на атаку
    {
        if (countDown <= 0f)
        {
            FaceTarget();
            if (distanceToTarget >= navMeshAgent.stoppingDistance)
            {
                ChaseTarget();
            }
            if (distanceToTarget <= navMeshAgent.stoppingDistance)
            {
                AttackTarget();
            }
        }
        else { return; }
    }

    public void AttackTarget()  // атака игрока
    {        
        GetComponent<Animator>().SetBool("attack",true);
    }

    private void FaceTarget()  // позиция к игроку
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);
    }
    private void ChaseTarget() // движение и атака мобов
    {
        GetComponent<Animator>().SetBool("attack", false);
        GetComponent<Animator>().SetTrigger("move");
        if (health.hitPoints > 0)
            navMeshAgent.SetDestination(target.position);
        else
            navMeshAgent.enabled = false;
    }

    void OnDrawGizmosSelected() // радиус активации триггера атаки
    {
        Gizmos.color = new Color(1, 1, 0, 0.75f);
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
