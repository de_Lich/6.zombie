﻿using System;
using UnityEngine;

public class EnemyHealth : MonoBehaviour  
{
    [SerializeField] public float hitPoints = 100f;
    [SerializeField] GameObject[] ammo;
    [SerializeField] EnemyType enemyType;
    public int enemyKills = 0;

    bool isDead = false;

    public bool IsDead()
    {
        return isDead;
    }

    public void TakeDamage(float damage) // получение урона
    {
        BroadcastMessage("OnDamageTaken", damage);
        hitPoints = hitPoints-damage;
        if(hitPoints <= 0)
        {
            Die();
        }
    }

    public void Die() // смерть моба
    {
        FindObjectOfType<AudioManager>().Play("ZombieDeath");
        EnemySpawn enemyscore = transform.parent.GetComponent<EnemySpawn>();
        enemyscore.EnemyScore(enemyType);
        if (isDead) return;
        isDead = true;
        GetComponent<Animator>().SetTrigger("die");
        DropFromEnemy();
    }

    private void DropFromEnemy()
    {
        int randomNumber = UnityEngine.Random.Range(1, 101);

        if(randomNumber <=20)
        Instantiate(ammo[UnityEngine.Random.Range(0, ammo.Length)], transform.position, Quaternion.identity);
    } // дроп из моба
}
