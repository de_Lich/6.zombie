﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPower : MonoBehaviour  // улучшение оружия
{
    [SerializeField] float damageIncrease = 5;
    [SerializeField] AmmoType ammoType;

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            FindObjectOfType<Weapon>().IncreaseDamage(ammoType, damageIncrease);
            Destroy(gameObject);
            Debug.Log("Bang");
        }
    }
}
