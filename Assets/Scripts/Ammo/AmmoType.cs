﻿public enum AmmoType
{
    BulletsPistol,
    ShotgunShell,
    SniperShots,
    BulletsAutomat,
    Rockets,
    Granades
}
