﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour   // система боеприпасов
{
    [SerializeField] AmmoSlot[] ammoSlots;

    [System.Serializable]
    private class AmmoSlot
    {
        public AmmoType ammoType;
        public int ammoAmount;
    }
    public void ReduceCurrentAmmo(AmmoType ammoType) // уменьшение 
    {
        GetAmmoSlot(ammoType).ammoAmount--;
    }
    public void IncreaseCurrentAmmo(AmmoType ammoType, int ammoAmount) // увеличение
    {
        GetAmmoSlot(ammoType).ammoAmount += ammoAmount;
    }
    public int GetCurrentAmmo(AmmoType ammoType) 
    {
        return GetAmmoSlot(ammoType).ammoAmount;
    }
    private AmmoSlot GetAmmoSlot(AmmoType ammoType) // какие боеприпасы к какому оружию
    {
        foreach(AmmoSlot slot in ammoSlots)
        {
            if(slot.ammoType == ammoType)
            {
                return slot;
            }
        }
        return null;
    }
}
