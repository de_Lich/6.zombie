﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] float healthPoints = 100f;
    [SerializeField] float delay = 2f;
    [SerializeField] TextMeshProUGUI healthText;
    [SerializeField] HealthBar healthBar;

    private void Start()
    {
        healthBar.SetMaxHealth(healthPoints);
    }
    public void TakeDamage(float damage) // механика получения урона
    {
        healthPoints = healthPoints - damage;
        if (healthPoints <= 0)
        {
            healthPoints = 0f;
            Invoke("KillPlayer", delay);
        }
        DisplayHealth();
    }

    private void DisplayHealth()  // отображение ХП
    {
        float currentHealth = healthPoints;
        healthText.text = currentHealth.ToString();
        healthBar.SetHealth(healthPoints);
    }

    public void KillPlayer() // смерть игрока
    {
        gameObject.GetComponent<DeathHandler>().HandledDeath();
    }
}
